# Master decider!

This application will help you to make important decisions in life!  
Every day you vote whether you feel like doing the thing or not - after quite some time you'll see your average attitude! I came up with the idea when I tried to decide whether to pursue Master's Degree or not.

# How to get it?

This is an AndroidStudio project - clone it and build it using AndroidStudio.

# Enjoy!

<img src="decider.png" alt="Application preview" width="200"/>

# Credits

During development I used some graphics, here are the credits:

- Icons made by <a href="https://www.flaticon.com/authors/vectors-market" title="Vectors Market">Vectors Market</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>
