package com.example.master_decider

import android.app.Activity
import android.os.Bundle
import com.example.master_decider.model.DecisionHelper
import com.example.master_decider.view.DecisionView
import com.example.master_decider.view.ResultsView

class MainActivity : Activity() {
    val decisionHelper = DecisionHelper(this)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        updateViews()
    }

    override fun onPostResume() {
        super.onPostResume()
        updateViews()
    }


    private fun updateViews() {
        val resultsView = ResultsView(this)
        val decisionView = DecisionView(this, resultsView::update)
        decisionView.update()
        resultsView.update()
    }
}
