package com.example.master_decider.model

import android.util.Log

class Results(val yays: Int, val nays: Int) {

    companion object Factory {
        fun load(helper: DecisionHelper): Results {
            var yays = 0
            var nays = 0

            for (decision in helper.getAll()) {
                if (decision.outcome == Variants.YAY)
                    yays += 1
                else if (decision.outcome == Variants.NAY)
                    nays += 1
            }

            Log.d("INFO", "Loaded yays: $yays Loaded nays: $nays")

            return Results(yays, nays)
        }
    }

}
