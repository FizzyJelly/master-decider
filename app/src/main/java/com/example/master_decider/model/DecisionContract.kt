package com.example.master_decider.model

import android.provider.BaseColumns

object DecisionContract {

    object DecisionEntry: BaseColumns {
        const val TABLE_NAME = "decisions"
        const val COLUMN_NAME_DATE = "date"
        const val COLUMN_NAME_OUTCOME = "outcome"
    }

}