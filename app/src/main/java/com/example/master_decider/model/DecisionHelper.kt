package com.example.master_decider.model

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import java.time.Instant
import java.util.*

private const val SQL_CREATE_TABLE =
    "CREATE TABLE ${DecisionContract.DecisionEntry.TABLE_NAME} (" +
            "${DecisionContract.DecisionEntry.COLUMN_NAME_DATE} INTEGER PRIMARY KEY," +
            "${DecisionContract.DecisionEntry.COLUMN_NAME_OUTCOME} INTEGER)"

private const val SQL_DELETE_TABLE =
    "DROP TABLE IF EXISTS ${DecisionContract.DecisionEntry.TABLE_NAME}"


class DecisionHelper(context: Context) :
    SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        const val DATABASE_VERSION = 1
        const val DATABASE_NAME = "MasterDecider.db"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(SQL_CREATE_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL(SQL_DELETE_TABLE)
        onCreate(db)
    }


    private fun getDescendingCursor(): Cursor {
        val db = readableDatabase

        val projection = arrayOf(
            DecisionContract.DecisionEntry.COLUMN_NAME_DATE,
            DecisionContract.DecisionEntry.COLUMN_NAME_OUTCOME
        )

        val sortOrder = "${DecisionContract.DecisionEntry.COLUMN_NAME_DATE} DESC"

        return db.query(
            DecisionContract.DecisionEntry.TABLE_NAME,
            projection,
            null,
            null,
            null,
            null,
            sortOrder
        )
    }

    private fun readNextDecision(cursor: Cursor): Decision? {
        with(cursor) {
            if (moveToNext()) {
                val date =
                    getLong(getColumnIndexOrThrow(DecisionContract.DecisionEntry.COLUMN_NAME_DATE))
                val outcome =
                    if (getInt(getColumnIndexOrThrow(DecisionContract.DecisionEntry.COLUMN_NAME_OUTCOME)) == 0) Variants.NAY else Variants.YAY

                return Decision(Date.from(Instant.ofEpochSecond(date)), outcome)
            }
        }
        return null
    }

    fun getLatest(): Decision? {
        return readNextDecision(getDescendingCursor())
    }

    fun getAll(): List<Decision> {
        val cursor = getDescendingCursor()
        val decisions = mutableListOf<Decision>()

        var decision = readNextDecision(cursor)
        while (decision != null) {
            decisions.add(decision)
            decision = readNextDecision(cursor)
        }

        return decisions

    }

    fun save(decision: Decision) {
        val outcome = if (decision.outcome == Variants.YAY) 1 else 0
        val date = decision.date.toInstant().epochSecond

        val values = ContentValues().apply {
            put(DecisionContract.DecisionEntry.COLUMN_NAME_DATE, date)
            put(DecisionContract.DecisionEntry.COLUMN_NAME_OUTCOME, outcome)
        }

        val db = writableDatabase
        val newRowId = db?.insert(DecisionContract.DecisionEntry.TABLE_NAME, null, values)
        if (newRowId?.compareTo(-1) == 0) {
            Log.d("INFO", "Saving record failed!")
        } else {
            Log.d("INFO", "Decision saved!")
        }

    }
}