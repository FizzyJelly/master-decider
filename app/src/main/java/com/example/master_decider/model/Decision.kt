package com.example.master_decider.model

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import java.sql.Timestamp
import java.time.Instant
import java.time.ZoneId
import java.util.*

enum class Variants {
    YAY, NAY
}

private fun zeroTime(date: Date): Date {
    val calendar = Calendar.getInstance()

    calendar.time = date
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);

    return calendar.time
}

class Decision(val date: Date, val outcome: Variants) {

    fun wasRecordedToday(): Boolean {
        val decisionDate = zeroTime(date)
        val now = zeroTime(Date.from(Calendar.getInstance().toInstant()))

        Log.d("INFO", "$now, $decisionDate")
        return now.compareTo(decisionDate) == 0
    }

    companion object Factory {
        fun make(outcome: Variants): Decision {
            return Decision(Date(), outcome)
        }
    }

}
