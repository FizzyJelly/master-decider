package com.example.master_decider.view

import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.example.master_decider.MainActivity
import com.example.master_decider.R
import com.example.master_decider.model.Decision
import com.example.master_decider.model.Variants

class DecisionView(
    val context: MainActivity,
    val decisionRecordedCb: () -> Unit
) {
    val prompt: TextView = context.findViewById(R.id.deciderMessage)
    val votingArea: LinearLayout = context.findViewById(R.id.reactionButtons)
    val yayButton: Button = context.findViewById(R.id.yayButton)
    val nayButton: Button = context.findViewById(R.id.nayButton)

    init {
        yayButton.setOnClickListener {
            recordDecision(Variants.YAY)
            update()
        }

        nayButton.setOnClickListener {
            recordDecision(Variants.NAY)
            update()
        }
    }

    fun update() {
        var latestDecision = context.decisionHelper.getLatest()
        if (latestDecision != null) {
            if (latestDecision.wasRecordedToday()) {
                hideVoting()
            } else {
                showVoting()
            }
        } else {
            showVoting()
        }
    }

    private fun hideVoting() {
        prompt.text = context.getString(R.string.alreadyRecordedMessage)
        votingArea.visibility = View.GONE
    }

    private fun showVoting() {
        prompt.text = context.getString(R.string.defaultMessage)
        votingArea.visibility = View.VISIBLE
    }


    private fun recordDecision(outcome: Variants) {
        context.decisionHelper.save(Decision.make(outcome))
        decisionRecordedCb()
    }
}