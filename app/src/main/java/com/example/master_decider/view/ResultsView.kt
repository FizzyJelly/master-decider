package com.example.master_decider.view

import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.example.master_decider.MainActivity
import com.example.master_decider.R
import com.example.master_decider.model.Results

class ResultsView(val context: MainActivity) {

    var resultsToggled: Boolean = false
    val toggleResultsButton: Button = context.findViewById(R.id.toggleResultsButton)
    val resultsArea: LinearLayout = context.findViewById(R.id.resultsIndicators)
    val reactionImage: ImageView = context.findViewById(R.id.resultsImage)
    val yayCounter: TextView = context.findViewById(R.id.yayCounter)
    val nayCounter: TextView = context.findViewById(R.id.nayCounter)

    init {
        toggleResultsButton.setOnClickListener {
            if (resultsToggled) {
                resultsToggled = false
                toggleResultsButton.text = context.getString(R.string.showResultsButtonText)
                resultsArea.visibility = View.GONE
                reactionImage.visibility = View.GONE
            } else {
                resultsToggled = true
                update()
                toggleResultsButton.text = context.getString(R.string.hideResultsButtonText)
                resultsArea.visibility = View.VISIBLE
                reactionImage.visibility = View.VISIBLE
            }
        }
    }

    fun update() {
        val currentResults = Results.load(context.decisionHelper)

        yayCounter.text = currentResults.yays.toString()
        nayCounter.text = currentResults.nays.toString()

        if (currentResults.yays > currentResults.nays) {
            reactionImage.setImageResource(R.drawable.winner)
        } else if (currentResults.yays < currentResults.nays) {
            reactionImage.setImageResource(R.drawable.puke)
        } else {
            reactionImage.setImageResource(R.drawable.neutral)
        }
    }

}